# Generated by Django 4.1.5 on 2023-01-08 05:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_alter_category_deletetime_alter_category_icon_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newsimg',
            name='parent',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='newsImg', to='users.news'),
        ),
    ]
