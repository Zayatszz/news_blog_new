from django.shortcuts import render
from rest_framework.generics import CreateAPIView
from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated, AllowAny
from .serializers import RegisterUserSerializer, UserSerializer
from rest_framework.decorators import api_view
from rest_framework import exceptions
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.viewsets import ModelViewSet
import requests


# add
from django.db.models import Count, Sum, Max, Min, Avg,  F, Q
from datetime import date, datetime
from users.pagination import CustomPageNumberPagination


from rest_framework import viewsets
from .serializers import CategorySerializer, NewsSerializer, NewsImgSerializer, BannerSerializer, CommentSerializer
from .models import Category, News, NewsImg, Banner, Comment


class RegisterUserView(CreateAPIView):
    queryset = get_user_model().objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterUserSerializer
    
@api_view(['POST'])
def login(request):
    email = request.data.get('email')
    password = request.data.get('password')
    
    user = get_user_model().objects.filter(email=email).first()
    if user is None:
        raise exceptions.AuthenticationFailed('User not found!')
    if not user.check_password(password):
        raise exceptions.AuthenticationFailed('Incorrect Password!')
    
    response = Response()
    
    token_endpoint = reverse(viewname='token_obtain_pair', request=request)
    tokens = requests.post(token_endpoint, data=request.data).json()
    
    response.data = {
        'access_token': tokens.get('access'),
        'refresh_token': tokens.get('refresh'),
        'email': user.email
    }
    
    return response
    


class CurrentLoggedInUser(ModelViewSet):
    queryset = get_user_model().objects.all()
    permission_classes = (IsAuthenticated, )
    serializer_class = UserSerializer
    
    def retrieve(self, request, *args, **kwargs):
        user_profile = self.queryset.get(email=request.user.email)
        serializer = self.get_serializer(user_profile)
        return Response({'user': serializer.data})
    
class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    
class NewsViewSet(viewsets.ModelViewSet):
    
    serializer_class = NewsSerializer
    pagination_class = CustomPageNumberPagination
    queryset = News.objects.all()

  
  
# tur commentlov
# class StandardPagesPagination(PageNumberPagination):
#       page_size = 10
# class NewsList(generics.ListAPIView):
#     # queryset = Oyunlar.objects.all()
#     pagination_class = StandardPagesPagination
#     filter_backends = [DjangoFilterBackend]
#     filterset_fields = ['news', 'platform']
#     # serializer_class = OyunlarSerializer
#     def get_queryset(self):
#         queryset=News.objects.all()
#         oyunlar=NewsSerializer.setup_eager_loading(queryset)

#         return oyunlar
#     def get(self,request,*args,**kwargs):

#         queryset = self.filter_queryset(self.get_queryset())
#         serializer=NewsSerializer(queryset,many=True)
#         page=self.paginate_queryset(serializer.data)
#         return self.get_paginated_response(page)


# def NewsList(request):
#     page = 1
#     pagecount = 50
#     data = News.objects.filter(Q(archive=False) & Q(delete__isnull=True) & Q(Q(published__isnull = True) | Q(published__lte=datetime.now()))  ).order_by("-pk")
#     if 'types' in request.GET:
#         if request.GET['types'] != "":
#             data = data.filter(types = int(request.GET['types']))
#     if 'pagecount' in request.GET:
#         if request.GET['pagecount'] != "":
#             pagecount = int(request.GET['pagecount'])
#     if 'page' in request.GET:
#         if request.GET['page'] != "":
#             page = int(request.GET['page'])
#     data = data[(page-1)*pagecount:(page)*pagecount]
#     return JsonResponse(NewsSerializerForm(data, many=True).data, safe=False)


class NewsImgViewSet(viewsets.ModelViewSet):
    
    serializer_class = NewsImgSerializer
    queryset = NewsImg.objects.all()

class BannerViewSet(viewsets.ModelViewSet):
    
    serializer_class = BannerSerializer
    queryset = Banner.objects.all()
class CommentViewSet(viewsets.ModelViewSet):
    
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
   