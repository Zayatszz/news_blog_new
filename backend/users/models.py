from django.db import models
from datetime import datetime
from django.contrib.auth.models import AbstractUser, BaseUserManager, PermissionsMixin
from ckeditor.fields import RichTextField



class CustomUserManager(BaseUserManager):
    
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None):
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user
    
    
class User(AbstractUser, PermissionsMixin):
    email = models.CharField(max_length=200, unique=True)
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    password = models.CharField(max_length=200)
    
    username = None
    
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['password']
    
    objects = CustomUserManager()
    
    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

class Category(models.Model):
    parent = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=40, null=True, blank=True, verbose_name="нэр")
    description = models.CharField(max_length=250, null=True, blank=True, verbose_name="тайлбар")
    icon = models.FileField(null=True, blank=True, verbose_name="зураг")
    

    order = models.IntegerField(null=True, blank=True, verbose_name="дараалал")
    deleteTime=models.DateTimeField(null=True, blank=True, verbose_name="устгасан хугацаа")
    def __str__(self):
        if self.parent is None:
            return self.name
        else:
            return self.parent.name + " | " +self.name
    
    class Meta:
        app_label = 'users'
        verbose_name =u'Мэдээний ангилал'
        verbose_name_plural = u'Мэдээний ангилал'


class News(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="хэрэглэгч")
    title = models.CharField(max_length=40, verbose_name="гарчиг")
    # picture = models.FileField(null=True, verbose_name="зураг")
    content= RichTextField(null=True, blank=True, verbose_name="дэлгэрэнгүй мэдээлэл")
    specialNews= models.BooleanField(default=False)
    createDate = models.DateTimeField(auto_now=True, verbose_name="Үүсгэсэн огноо")
    publishDate = models.DateTimeField(null=True, blank=True, verbose_name="Нийтэлсэн огноо")
    view = models.IntegerField(default=0,  verbose_name="Үзсэн тоо")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name="категори" )
    deleteDate = models.DateTimeField(null=True, blank=True, verbose_name="устгасан хугацаа") 

    def __str__(self):
        return self.title

    # def __unicode__(self):
    #     return '%s' % (self.user.email)
       
    
    class Meta:
        app_label = 'users'
        verbose_name =u'Мэдээ'
        verbose_name_plural = u'Мэдээ'

class NewsImg(models.Model):
    parent=models.ForeignKey(News, related_name='newsImg', on_delete=models.CASCADE)
    description = models.CharField(max_length=200, null=True, blank=True, verbose_name="Зурагны тайлбар")
    types = models.IntegerField(default=1, choices=((1, u'зураг'), (2, u'видео'), (3, u'бусад')), verbose_name="Төрөл")
    file = models.FileField(upload_to='news', verbose_name=u'зураг', null=True, blank=True)
    def __unicode__(self):
        return '%s' % (self.description)
   
    class Meta:
        verbose_name = u'Мэдээний зураг, файл'
       
class Comment(models.Model):
    news = models.ForeignKey(News, related_name='comment', on_delete=models.CASCADE, verbose_name="Мэдээ")
    title = models.CharField(max_length=80, null=True, blank=True, verbose_name="Гарчиг")
    email = models.EmailField(verbose_name="Имэйл")
    body = RichTextField(verbose_name="Коммент")
    created_on = models.DateTimeField(auto_now_add=True, verbose_name="Бичсэн хугацаа")
    active = models.BooleanField(default=False, verbose_name="Идэвх")

    class Meta:
        ordering = ['created_on']

    def __str__(self):
        return ' {} by {}'.format(self.news, self.title)
        
class Banner(models.Model):
    user= models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="хэрэглэгч")
    title = models.CharField(null=True, blank=True, max_length=80, verbose_name="Гарчиг")
    link = models.CharField(null=True, blank=True,  max_length=80, verbose_name="Холбоос")
    position = models.IntegerField(default=0, choices=((0, u'байрлал1'), (1, u'байрлал2'), (2, u'байрлал3'), (3, u'байрлал4'), (4, u'байрлал5'), (5, u'байрлал6'), (6, u'байрлал7'), (7, u'байрлал8'), (8, u'байрлал9'), (9, u'байрлал10'), ), verbose_name="Байршил")
    active = models.BooleanField(default=True, verbose_name="Идэвх")
    image = models.FileField(null=True, blank=True)
    description = models.CharField(max_length=250, null=True, blank=True, verbose_name="тайлбар")
    price = models.IntegerField(default=0, null=True, blank=True)